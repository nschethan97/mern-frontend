import React from 'react';
import {Link} from 'react-router-dom';
import { Nav, NavItem, Dropdown, DropdownItem, DropdownToggle, DropdownMenu, NavLink } from 'reactstrap';

export default class Redirectfront extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  render() {
    return (
      <div>
        <Nav pills>
          <NavItem>
            <NavLink href="#" active className="nav-item">Sigma</NavLink>
          </NavItem>
        
          <NavItem>
            <NavLink href="#"><Link to='/leavestatus' className="nav-item">Leave Status</Link></NavLink>
          </NavItem>
          <NavItem>
            <NavLink href="#"><Link to='/redirect' className="nav-item">Request Leave</Link></NavLink>
         
          </NavItem>
          
          {/* <NavItem>
            <NavLink href="#"><Link to='/mysetting' className="nav-item"><i class="fa fa-gear" ></i>MySetting</Link></NavLink>
         
          </NavItem> */}
          <NavItem>
            <NavLink href="#"><Link to='/frontpage' className="nav-item"><i class="fa fa-sign-out" ></i>Log Out</Link></NavLink>
         
          </NavItem>
         
        </Nav>
      </div>
    );
  }
}