import React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
//  import "./Redirectpage.css"
import axios from 'axios'

export default class Redirectpage extends React.Component {
  constructor(props) {
    super(props)
  
    this.state = {
       leavetype:'',
       fromdate:'',
       todate:'',
       remarks:''
    }
  }
  changehandler=(e,fieldName)=>{
    console.log('fieldname:'+fieldName);
    console.log('console:'+e.target.value);
    if(fieldName==="leavetype"){
      this.setState({
        leavetype:e.target.value
      })
    }
    if(fieldName==="fromdate"){
      this.setState({
        fromdate:e.target.value
      })
    }
    if(fieldName==="todate"){
      this.setState({
        todate:e.target.value
      })
    }
    if(fieldName==="remarks"){
      this.setState({
        remarks:e.target.value
      })
    }

  }
  changesubmit=(event)=>{
        
    event.preventDefault();
    const {leavetype,fromdate,todate,remarks}=this.state
    const data={
        
       leavetype:leavetype,
       fromdate:fromdate,
       todate:todate,
       remarks:remarks,
       status:"Pending"
    }
    
    axios.post("http://localhost:5000/users/add",data)
    .then(res=>{
      // console.log(res))
      // console.log(`${this.state.leavetype} ${this.state.fromdate} ${this.state.todate}${this.state.remarks}`)
        if (res.status===200) {
             window.location.href='/redirectfront'
            // alert("LoginSuccssful")
            
        } else {
            alert("Login Failed")
        }
    })
    .catch(err=>console.log(err)
     ) 
    console.log(` ${this.state.leavetype}${this.state.fromdate}${this.state.todate}${this.state.remarks}`)
   
}
  
  render() {
    return (
    <div className="bd">
      <Form className="bd"  onSubmit={this.changesubmit}>
       <FormGroup>
          <Label for="exampleSelect">Leave Type</Label>
          <Input type="select" name="leavtype" id="exampleSelect" value={this.state.leavetype} onChange={(e)=>this.changehandler(e,"leavetype")} required>
            <option>Sick Leave</option>
            <option>Casual Leave</option>
            <option>Maternity Leave</option>
            <option>Study Leave</option>
            <option>Transfer Leave</option>
          </Input>
        </FormGroup>
       
        
        <FormGroup>
          <Label for="exampleDate"><i class="fa fa-calendar"></i> From Date</Label>
          <Input
            type="date"
            name="fromdate"
            id="exampleDate"
            placeholder="date placeholder"
            value={this.state.fromdate}
            onChange={(e)=>this.changehandler(e,"fromdate")}
          />
        </FormGroup>
        <FormGroup>
          <Label for="exampleDate"><i class="fa fa-calendar" aria-hidden="true"></i> To Date</Label>
          <Input
            type="date"
            name="todate"
            id="exampleDate"
            placeholder="date placeholder"
            value={this.state.todate}
            onChange={(e)=>this.changehandler(e,"todate")}
            required
          />
        </FormGroup>
        
      
        <FormGroup>
          <Label for="exampleText">Remarks</Label>
          <Input type="textarea" name="remarks" id="exampleText"  value={this.state.remarks} required onChange={(e)=>this.changehandler(e,"remarks")}/>
        </FormGroup>
       
        
        
        <Button>Submit</Button>
      </Form>
      </div>
    );
  }
}