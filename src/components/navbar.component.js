import React,{Component} from 'react';
import {Link} from 'react-router-dom';
// import { FontAwesomeIcon } from 'react-fontawesome';
// import { faThumbsUp } from 'react-fontawesome';
// var FontAwesome = require('react-fontawesome');
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem } from 'reactstrap';
 export class Navigation extends Component{
     
    constructor(props) {
        super(props);
    
        this.toggle = this.toggle.bind(this) ;
        this.state = {
          isOpen: false
        };
      }
      toggle() {
        this.setState({
          isOpen: !this.state.isOpen
        });
      }
    render(){
        return(     
                    <div className="navbr">
                     <Navbar color="dark"  expand="md">
                      <NavbarBrand href="" color="danger" ><Link to='/frontpage' className="nav-item"><i className="fa fa-yelp" ></i>Elekta</Link></NavbarBrand>
                    
                      <h1 class="navbar-text" className="t">Leave Management System</h1> 
                      <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isO} navbar>
                     <Nav className="ml-auto" navbar>                    
                    <NavItem className="nav-item"><NavLink href="https://www.elekta.com/" className="nav-item"><i className="fa fa-home"></i>&nbsp;About Us</NavLink>
                    </NavItem>
                    <NavItem className="nav-item">
                <NavLink href=""><Link to='/create' className="nav-item"><i class="fa fa-user-plus" ></i>SignUp</Link></NavLink>
              </NavItem>
                    <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret className="nav-item"><i className="fa fa-sign-in"></i>
                &nbsp;SignIn
                  
                </DropdownToggle>
                <DropdownMenu right className="nav-item">
                  <DropdownItem className="nav-item">
                    <Link to='/user'><i className="fa fa-users"></i>User</Link>
                  </DropdownItem>
                  <DropdownItem className="nav-item">
                  <Link to='/admin'><i class="fa fa-user"></i>Admin </Link>
                  </DropdownItem>
                  
                </DropdownMenu>
              </UncontrolledDropdown>
              </Nav>  
              </Collapse>
              </Navbar>
              
            </div>
        
        )
    }
}
export default Navigation

//  <nav className="navbar navbar-dark bg-dark navbar-expand-lg">
//                <Link to="/" className="navbar-brand">SIGMA</Link>
//                <div className="collpase navbar-collapse">
//                     <ul className="navbar-nav ml-auto">
//                         <li className="navbar-item">
//                         <Link to="/" className="nav-link">About Us</Link></li>
//                         <li className="navbar-item">
//                         <Link to="/create" className="nav-link">Sign Up</Link></li>
//                         <li className="navbar-item">
//                         <Link to="/user" className="nav-link">Sign In</Link></li>
//                     </ul>