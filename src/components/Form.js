import React, { Component } from 'react';
import validator from 'validator';
import axios from 'axios';
 import "./Form.css";   
import Image from "./logo.png";
import {Link} from 'react-router-dom';
export class Form extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             firstname : '',
             lastname : '',
             email : '',
             password : '', 
             role_type: 'user'
            
        }
    }
// export class Form extends Component {
//     state = {
//                      username : '',
//                      lastname : '',
//                      email : '',
//                      password : ''
//                 }
//     changeHandler= event =>
//     {
//        const isChechbox=event.target.type === "checkbox";
//           this.setState({
//               [event.target.name]:isChechbox
//               ?event.target.checked:
//               event.target.value
//           });
//     };
    changeHandler=(e,fieldName)=>{
        console.log("fieldName:"+fieldName)
        console.log("console: "+e.target.value)
        if(fieldName === "FIRSTNAME"){
        this.setState({
            firstname : e.target.value
              })
              
       }
       if(fieldName === 'LASTNAME'){
           this.setState({
               lastname: e.target.value
           })
       }
       if(fieldName === 'EMAIL'){
        this.setState({
            email: e.target.value
        })
        }
        if(fieldName === 'PASSWORD'){
             this.setState({
                password: e.target.value
        })
    }
    if(fieldName === 'ROLE_TYPE'){
        this.setState({
        role_type : e.target.value
   })
}
        
        }
    // changeHandlerrr=(event)=>{
    //     this.setState({
    //         lastname : event.target.value
    //     })
    // }
    // changePasswordHandler=(event)=>{
    //     this.setState({
    //         password : event.target.value
    //     })
    // }
    // changeHandlerr=(event)=>{
    //     this.setState({
    //         email : event.target.value
    //     })
    // }
    changesubmit=(event)=>{
        event.preventDefault();
        const {firstname,lastname,email,password,role_type}=this.state
        const data={
            firstname:firstname,
            lastname:lastname,
            
            email :email,
            password :password,
            role_type :role_type,
        }
            
        axios.post("http://localhost:5000/exercises/add",data)
        
        .then(res=>{
            if (res.status===200) {
                window.location.href='/frontpage'
                alert("SignUp Successful")
                
            } else {
                alert("SignUp Failed")
            }
        })
        .catch(err=>console.log(err)
         ) 
        console.log(`${this.state.firstname} ${this.state.lastname} ${this.state.email}`)
        
    }
   
    // submit = () =>{
       
    //     if(validator.isEmail(this.state.email)){
    //         console.log("true")
    //     }
    //     else{
    //         console.log("Invalid Email")
    //     }
       
    // };

    render() {
        return (
            <div className="abc">
           
            <form onSubmit={this.changesubmit} action="/action_page.php" autocomplete="on">
            <div>
            <h4 >Registration <img src={Image} className="avatar"/><span className="spanclass"></span></h4>
             
            
            <div >
              <label className="a">FirstName:</label>  <br></br>
               <input type="text" value={this.state.firstname} placeholder="FirstName" size="37" required minLength="3"  onChange={(e)=>this.changeHandler(e,"FIRSTNAME")} ></input>
             </div>
            <div>
              <label className="a">LastName:</label> <br></br>
               <input type="text" value={this.state.lastname} placeholder="LastName" size="37" minLength="3" onChange={(e)=>this.changeHandler(e,"LASTNAME") } required></input>
            </div>
            <div>
              <label className="a">  Email:</label>  <br></br>
              
               <input type="email" value={this.state.email} placeholder="Email" size="37" onChange={(e)=>this.changeHandler(e,"EMAIL") } required></input>
            </div>
            <div>
            <label className="a">Password:</label> <br></br>
            <input type="password" value={this.state.password} placeholder="Password" size="37" onChange={(e)=>this.changeHandler(e,"PASSWORD")} required></input>
            
            </div>  
            <div >
                <label className="a">role_type:</label>  <br></br>
                <select
                   type="text"
                   value={this.state.role_type} placeholder="Password" onChange={(e)=>this.changeHandler(e,"ROLE_TYPE")} required
                >
                 
                    <option>User</option>
                    <option>Admin</option>
                </select>
            </div>
            
            <button type="submit" >Submit</button><br></br>
            <Link to="/user">Already Registered Login !</Link>
            </div>
            </form>
            </div>
        )
    }
}

export default Form
