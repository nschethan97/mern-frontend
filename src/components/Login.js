import React, { Component } from 'react';
import validator from 'validator';
import axios from 'axios'
import {Link} from 'react-router-dom';
import "./Form.css";   
import Image from "./logo4.png";
import auth from "./auth";


export class Login extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             
             email : '',
             password : ''
            
        }
    }
    
// export class Form extends Component {
//     state = {
//                      username : '',
//                      lastname : '',
//                      email : '',
//                      password : ''
//                 }
//     changeHandler= event =>
//     {
//        const isChechbox=event.target.type === "checkbox";
//           this.setState({
//               [event.target.name]:isChechbox
//               ?event.target.checked:
//               event.target.value
//           });
//     };
    changeHandler=(e,fieldName)=>{
        console.log("fieldName:"+fieldName)
        console.log("console: "+e.target.value)
       
       if(fieldName === 'EMAIL'){
        this.setState({
            email: e.target.value
        })
        }
        if(fieldName === 'PASSWORD'){
             this.setState({
                password: e.target.value
        })
    }
        
        }
    // changeHandlerrr=(event)=>{
    //     this.setState({
    //         lastname : event.target.value
    //     })
    // }
    // changePasswordHandler=(event)=>{
    //     this.setState({
    //         password : event.target.value
    //     })
    // }
    // changeHandlerr=(event)=>{
    //     this.setState({
    //         email : event.target.value
    //     })
    // }
    // componentDidMount()
    // {
    //     axios.get('https://jsonplaceholder.typicode.com/posts')
    //     .then(res=>console.log(res))
    // }
    changesubmit=(event)=>{
        
        event.preventDefault();
        const {email,password}=this.state
        const data={
            
            email:email,
            password:password
        }
        
        axios.post("http://localhost:5000/exercises/login",data)
        .then(res=>{
            auth.login(() => {
                if (res.status===200) {
                
                
                    if(res.data.role_type==="user")
                    {
                        // window.location.href='/redirectfront'
                        this. props.history.push("/redirectfront"); 
                    }
                    else
                    {
                        alert("You are not user")
                    }
                    
                } else {
                    alert("Login Failed")
                }
              });
           
        })
        .catch(err=>console.log(err)
         ) 
        console.log(` ${this.state.email}${this.state.password}`)
       
    }
    submit = () =>{
       
        if(validator.isEmail(this.state.email)){
            console.log("true")
        }
        else{
            console.log("Invalid Email")
        }
       
    };

    render() {
        return (
            <div className="abc">
            
            <form onSubmit={this.changesubmit} >
       
            <h4 >User<img src={Image} className="avatar"/><span className="spanclass"></span></h4>

            
            
            <div>
              <label className="a">  Email:</label>  <br></br>
              
               <input type="email" size="37" value={this.state.email} placeholder="Email" onChange={(e)=>this.changeHandler(e,"EMAIL")  } required></input>
            </div>
            <div>
            <label className="a">Password:</label> <br></br>
            <input type="password" size="37" value={this.state.password}  placeholder="Password" onChange={(e)=>this.changeHandler(e,"PASSWORD")} required></input>
            
            </div>  
            
            <button type="submit">Submit</button><br></br>
            <Link to='/create' className="login-bottom">create account !</Link>
            </form>
            </div>
        )
    }
}

export default Login
