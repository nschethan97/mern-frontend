import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { Button } from 'reactstrap';
import auth from "./auth";




export default class LeaveStatus extends Component {
  constructor(props) {
    super(props);

  
    this.state = {exercises: [],databyId:[],ID:'',
    _id:'',
    leavetype:'',
    fromdate:'',
    status:'',
    todate:'',
    remarks:''
  }
    
  }
 


  componentDidMount() {
    axios.get('http://localhost:5000/users/')
      .then(response =>
        {
          console.log(response.data);
          
        this.setState({ exercises: response.data })
      })
      .catch((error) => {
        console.log(error);
      })
  }


  
  

  render() {
 const {exercises}=this.state
        const data= exercises.map(currentexercise=> {
            return(
                <tr key={currentexercise._id}>
                    <td>{currentexercise.leavetype}</td>
                    <td>{currentexercise.fromdate}</td>
                    <td>{currentexercise.todate}</td>
                    <td>{currentexercise.remarks}</td>
                    <td>{currentexercise.status}</td>
                  
              </tr>)
            }
        )
    return (
      <div>
      {/* <Button color="warning" className="buttoninleaveapproval"><Link to ="/frontpage"> LogOut</Link> </Button>{' '} */}
      <Button color="warning" className="buttoninleaveapproval" onClick={() => {
          auth.logout(() => {
            this.props.history.push("/user");
          });
        }}> LogOut</Button>{' '}
         <h3 >Leave Status</h3>

        <table className="table">
          <thead className="thead-light">
            <tr>
              <th>LeaveType</th>
              <th>From Date</th>
              <th>ToDate</th>
              <th>Remarks</th>
            <th>Status</th>
            </tr>
          </thead>
          <tbody>
            {data}
          </tbody>
        </table>
      </div>
    )
  }
}