import React from 'react';
import {BrowserRouter as Router,Route} from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css"

import {Navigation} from "./components/navbar.component";
import Form from './components/Form';
import Frontpage from './components/Frontpage';
import Login from './components/Login';
import Admin from './Admin/Login';
 import './App.css';
import Redirectpage from "./components/Redirectpage"
import Redirectfront from "./components/Redirectfront"
import LeaveStatus from "./components/LeaveStatus"
import AdminOperation from "./Admin/AdminOperation"
import AdminEdit from "./Admin/AdminEdit";
import Mysetting from "./components/Mysetting";
import Adminconnection from "./Admin/Adminconnection";
import LeaveApproval from "./Admin/LeaveApproval";
import { ProtectedRoute } from "./components/protected.route";
import { ProtectedRoute1} from "./Admin/protected.routes";
function App() {
  return (
   
    <Router>
    <div className="">
   
      <Navigation></Navigation> 
    <br></br>
     
      <Route path="/create" component={Form}></Route>
      <Route path="/user" exact component={Login}></Route>
      <Route path="/admin" exact component={Admin}></Route>
      {/* <Route path="/redirectfront" exact component={Redirectfront}></Route> */}
      {/* <Route path="/redirect" exact component={Redirectpage}></Route> */}
      {/* <Route path="/adminoperation" exact component={AdminOperation}></Route> */}
      {/* <Route path="/edit/:id" exact component={AdminEdit}></Route> */}
      <Route path="/mysetting" exact component={Mysetting}></Route>
      {/* <Route path="/adminconnection" exact component={Adminconnection}></Route> */}
      {/* <Route path="/leaveapproval" exact component={LeaveApproval}></Route> */}
      <Route path="/frontpage" exact component={Frontpage}></Route>
      {/* <Route path="/leavestatus" exact component={LeaveStatus}></Route> */}
      <ProtectedRoute exact path="/redirectfront" component={Redirectfront} />
      <ProtectedRoute exact path="/leavestatus" component={LeaveStatus} />
      <ProtectedRoute exact path="/redirect" component={Redirectpage} />
      <ProtectedRoute1 exact path="/adminconnection" component={Adminconnection} />
      <ProtectedRoute1 exact path="/leaveapproval" component={LeaveApproval} />
      <ProtectedRoute1 exact path="/adminoperation" component={AdminOperation} />
      <ProtectedRoute1 exact path="/edit/:id" component={AdminEdit} />
      
      
      
      
</div>
    </Router>
//  <div className="APP">
//    <Form></Form> 
//   <Login></Login>
//   <AdminEdit></AdminEdit>
// </div> 
  );
}

export default App;
