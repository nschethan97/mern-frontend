import React from "react";
import { Route, Redirect } from "react-router-dom";
import auth from "../components/auth";

export const ProtectedRoute1 = ({
  component: Component,
  ...rest
}) => {
  return (
    <Route
      {...rest}
      render={props => {
        if (auth.isAuthenticated()) {
          return <Component {...props} />;
        } else {
          return (
            <Redirect
              to={{
                pathname: "/admin",
                state: {
                  from: props.location
                }
              }}
            />
          );
        }
      }}
    />
  );
};
