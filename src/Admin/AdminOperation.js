import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { Button } from 'reactstrap';


const Exercise = props => (
  <tr>
    <td>{props.exercise.firstname}</td>
    <td>{props.exercise.lastname}</td>
    <td>{props.exercise.email}</td>
 
    <td>
    <Link to={`/edit/${props.exercise._id}` }>edit</Link> | <a href="#" onClick={() => { props.deleteExercise(props.exercise._id) }}>delete</a> | <Link to={"/create/"}>add</Link>
    </td>
  </tr>
)

export default class AdminOperation extends Component {
  constructor(props) {
    super(props);

    this.deleteExercise = this.deleteExercise.bind(this)

    this.state = {exercises: []};
  }

  componentDidMount() {
    axios.get('http://localhost:5000/exercises/')
      .then(response =>
        {
          console.log(response.data);
          
        this.setState({ exercises: response.data })
      })
      .catch((error) => {
        console.log(error);
      })
  }

  deleteExercise(id) {
    axios.delete(`http://localhost:5000/exercises/${id}`)
      .then(response => { console.log(response.data)});

    this.setState({
      exercises: this.state.exercises.filter(el => el._id !== id)
    })
  }

  exerciseList() {
    return this.state.exercises.map(currentexercise => {
      return <Exercise exercise={currentexercise} deleteExercise={this.deleteExercise} key={currentexercise._id}/>;
    })
  }

  render() {
    return (
      <div>
      <Button color="warning" className="buttoninleaveapproval"><Link to ="/frontpage"> LogOut</Link> </Button>{' '}
      <br></br>
        {/* <h3>Logged Exercises</h3> */}
        <table className="table">
          <thead className="thead-light">
            <tr>
              <th>FirstName</th>
              <th>LastName</th>
              <th>Email</th>
            
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            { this.exerciseList() }
          </tbody>
        </table>
      </div>
    )
  }
}
// import React from 'react';
// import { Button ,UncontrolledPopover, PopoverHeader, PopoverBody} from 'reactstrap';

// export default class AdminOperation extends React.Component {
//   render() {
//     return (
//       <div>
      
//         {' '}<Button id="PopoverFocus"  outline color="primary" className="button">Click</Button><br></br>
//         {' '} <Button outline color="secondary" className="button">Click</Button><br></br>
//         {' '} <Button outline color="success" className="button">Click</Button><br></br>
//         {' '}<Button outline color="info" className="button">Click</Button><br></br>
//         {' '}<Button outline color="warning" className="button">Click</Button><br></br>
//         {' '} <Button outline color="danger" className="button">Click</Button> 
//         <UncontrolledPopover trigger="focus" placement="bottom" target="PopoverFocus">
//           <PopoverHeader>Focus Trigger</PopoverHeader>
//           <PopoverBody>Focusing on the trigging element makes this popover appear. Blurring (clicking away) makes it disappear. You cannot select this text as the popover will disappear when you try.</PopoverBody>
//         </UncontrolledPopover>   
//       </div>
//     );
//   }
// }