import React, { Component } from 'react'
import axios from 'axios';
import {Link} from 'react-router-dom';
import   "../components/Form.css"
import Image from "./logo3.png";
import auth from "../components/auth";


export class Admin extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             email:"",
             password:""
        }
    }

    changehandler=(e,fieldname)=>{
        console.log("fieldname:"+fieldname);
        console.log("console:"+e.target.value);
        if (fieldname==='EMAIL'){
            this.setState({
                email:e.target.value
            })
        }
        if(fieldname=== 'PASSWORD'){
            this.setState({
                password:e.target.value
            })
        }
    }
    changesubmit=(event)=>{
        
        event.preventDefault();
        const {email,password}=this.state
        const data={
            
            email:email,
            password:password
        }
        
        axios.post("http://localhost:5000/exercises/login",data)
        .then(res=>{
             auth.login(() => {
                 
             
            if (res.status===200) {
               
                // alert("LoginSuccssful")
                if(res.data.role_type==="Admin")
                {
                    // window.location.href='/adminconnection'
                   this.props.history.push("/adminconnection");
                }
                else
                {
                    alert("You are not admin")
                }
            } else {
                alert("Login Failed")
            }
        });
        })
        .catch(err=>console.log(err)
         ) 
        console.log(` ${this.state.email}${this.state.password}`)
    
    }
    render() {
        return (
            <div>
                <form onSubmit={this.changesubmit} autoComplete="on">
                <h4 >Admin <img src={Image} className="avatar"/><span className="spanclass"></span></h4>
                <div>
                    <label>Email:</label><br></br>
                    <input type="email" size="37" value={this.state.email} placeholder="Email" onChange={(e)=>this.changehandler(e,'EMAIL')} required></input>
</div>
        <div>
            <label>Password:</label><br></br>
            <input type="password" size="37" value={this.state.password} placeholder="Password" onChange={(e)=>this.changehandler(e,'PASSWORD')} required></input>
        </div>
        <div>
            <button type="submit">Submit</button>
        </div>
                </form>

            </div>
        )
    }
}

export default Admin
