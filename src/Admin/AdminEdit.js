
import React, { Component } from 'react';
import validator from 'validator';
import axios from 'axios';
   

export class AdminEdit extends Component {
    constructor(props) {

       

        super(props)
    
        this.state = {
             firstname : '',
             lastname : '',
             email : '',
             password : ''
            
        }
    }
    componentDidMount() {
        
        axios.get('http://localhost:5000/exercises/'+this.props.match.params.id)
                 .then(response => {

                    console.log(response.data);
                   this.setState({
                      firstname: response.data.firstname,
                      lastname: response.data.lastname,
                        email: response.data.email,
                      password: response.data.password
                   })   
                 })
                 .catch(function (error) {
                    console.log(error);
             })
           
             
           
         }

    changeHandler=(e,fieldName)=>{
        console.log("fieldName:"+fieldName)
        console.log("console: "+e.target.value)
        if(fieldName === "FIRSTNAME"){
        this.setState({
            firstname : e.target.value
              })
              
       }
       if(fieldName === 'LASTNAME'){
           this.setState({
               lastname: e.target.value
           })
       }
       if(fieldName === 'EMAIL'){
        this.setState({
            email: e.target.value
        })
        }
        if(fieldName === 'PASSWORD'){
             this.setState({
                password: e.target.value
        })
    }
        
        }
        
    
    changesubmit=(event)=>{
        event.preventDefault();
        const {firstname,lastname,email,password}=this.state
        const data={
            firstname:firstname,
            lastname:lastname,
            
            email:email,
            password:password,
            // role_type:'user'
        }
        axios.put("http://localhost:5000/exercises/update/"+this.props.match.params.id,data)
        .then(res=>{
            if (res.status===200) {
                window.location.href='/edit/:id'
                alert("Exercises updated")
                
            } else {
                alert("updatation failed")
            }
        })
        .catch(err=>console.log(err)
         ) 
        console.log(`${this.state.firstname} ${this.state.lastname} ${this.state.email}`)
        
    }
    

    render() {
        return (
            <div className="abc">
           
            <form onSubmit={this.changesubmit} autoComplete="on">
       
            <h4 >CREATE ACCOUNT</h4>

            <div >
              <label className="a">FIRSTNAME:</label>  <br></br>
               <input type="text" value={this.state.firstname} placeholder="firstname" required minLength="3" onChange={(e)=>this.changeHandler(e,"FIRSTNAME")} ></input>
             </div>
            <div>
              <label className="a">LASTNAME:</label> <br></br>
               <input type="text" value={this.state.lastname} placeholder="lastname" minLength="3" onChange={(e)=>this.changeHandler(e,"LASTNAME") } required></input>
            </div>
            <div>
              <label className="a">  EMAIL:</label>  <br></br>
              
               <input type="email" value={this.state.email} placeholder="email" onChange={(e)=>this.changeHandler(e,"EMAIL") } required></input>
            </div>
            <div>
            <label className="a">PASSWORD:</label> <br></br>
            <input type="password" value={this.state.password} placeholder="password" onChange={(e)=>this.changeHandler(e,"PASSWORD")}required></input>
            
            </div>  
            
            <button type="submit" >submit</button>
            </form>
            </div>
        )
    }
}

export default AdminEdit

// import React, { Component } from 'react';
// import axios from 'axios';



// export default class AdminEdit extends Component {
//   constructor(props) {
//     super(props);
//     this.changeHandler= this.changeHandler.bind(this);
   
//     this.onSubmit = this.onSubmit.bind(this);
   
//     this.state = {
//       firstname: '',
//       lastname: '',
//       email: '',
//       password: ''
//     //   users: []
//     }
//   }
//   changeHandler=(e,fieldName)=>{
//     console.log("fieldName:"+fieldName)
//     console.log("console: "+e.target.value)
//     if(fieldName === "FIRSTNAME"){
//     this.setState({
//         firstname : e.target.value
//           })
          
//    }
//    if(fieldName === 'LASTNAME'){
//        this.setState({
//            lastname: e.target.value
//        })
//    }
//    if(fieldName === 'EMAIL'){
//     this.setState({
//         email: e.target.value
//     })
//     }
//     if(fieldName === 'PASSWORD'){
//          this.setState({
//             password: e.target.value
//     })
// }
    
//     }
//   componentDidMount() {
//     axios.get('http://localhost:5000/exercises/'+this.props.match.params.id)
//       .then(response => {
//         this.setState({
//           firstname: response.data.firstname,
//           lastname: response.data.lastname,
//         email: response.data.email,
//           password: response.data.password
//         })   
//       })
//       .catch(function (error) {
//         console.log(error);
//       })

  

//   }



//   onSubmit(e) {
//     e.preventDefault();

//     const exercise = {
//       firstname: this.state.firstname,
//       lastname: this.state.lastname,
//       email: this.state.email,
//       password: this.state.password
//     }

//     console.log(exercise);

//     axios.post('http://localhost:5000/exercises/update/' + this.props.match.params.id, exercise)
//       .then(res => console.log(res.data));

//     window.location = '/user';
//   }

//   render() {
//     return (
//     <div>
//       <h3>Edit Exercise Log</h3>
//       <form onSubmit={this.onSubmit}>
//         <div className="form-group"> 
//           <label>firstname: </label>
//           <input  type="text"
//               required
//               className="form-control"
//               value={this.state.firstname}
//               onChange={this.changeHandler}
//               />
      
//         </div>
//         <div className="form-group"> 
//           <label>LASTNAME: </label>
//           <input  type="text"
//               required
//               className="form-control"
//               value={this.state.lastname}
//               onChange={this.changeHandler}
//               />
//         </div>
//         <div className="form-group">
//           <label>EMAIL: </label>
//           <input 
//               type="email" 
//               className="form-control"
//               value={this.state.email}
//               onChange={this.changeHandler}
//               />
//         </div>
//         <div className="form-group">
//           <label>PASSWORD: </label>
//           <input 
//               type="password" 
//               className="form-control"
//               value={this.state.password}
//               onChange={this.changeHandler}
//               />
//         </div>

//         <div className="form-group">
//           <input type="submit" value="Edit Exercise Log" className="btn btn-primary" />
//         </div>
//       </form>
//     </div>
//     )
//   }
// }



// import React, { Component } from 'react';
// import axios from 'axios';
// // import DatePicker from 'react-datepicker';
// // import "react-datepicker/dist/react-datepicker.css";

// export default class AdminEdit extends Component {
//   constructor(props) {
//     super(props);

//     this.onChangeUsername = this.onChangeUsername.bind(this);
//     this.onChangeDescription = this.onChangeDescription.bind(this);
//     this.onChangeDuration = this.onChangeDuration.bind(this);
//     this.onChangeDate = this.onChangeDate.bind(this);
//     this.onSubmit = this.onSubmit.bind(this);

//     this.state = {
//       firstname: '',
//       lastname: '',
//       email: '',
//       password: ''
      
      
//     }
//   }

//   componentDidMount() {
//     axios.get('http://localhost:5000/exercises/'+this.props.match.params.id)
//       .then(response => {
//         this.setState({
//           firstname: response.data.firstname,
//           lastname: response.data.lastname,
//           email: response.data.email,
//           password: response.data.password
//         })   
//       })
//       .catch(function (error) {
//         console.log(error);
//       })

//     // axios.get('http://localhost:5000/users/')
//     //   .then(response => {
//     //     if (response.data.length > 0) {
//     //       this.setState({
//     //         users: response.data.map(user => user.username),
//     //       })
//     //     }
//     //   })
//     //   .catch((error) => {
//     //     console.log(error);
//     //   })

//   }

//   onChangeUsername(e) {
//     this.setState({
//       firstname: e.target.value
//     })
//   }

//   onChangeDescription(e) {
//     this.setState({
//       lastname: e.target.value
//     })
//   }

//   onChangeDuration(e) {
//     this.setState({
//       email: e.target.value
//     })
//   }

//   onChangeDate(e) {
//     this.setState({
//      password: e.target.value
//     })
//   }

//   onSubmit(e) {
//     e.preventDefault();

//     const exercise = {
//       firstname: this.state.firstname,
//       lastname: this.state.lastname,
//       email: this.state.email,
//       password: this.state.password
//     }

//     console.log(exercise);

//     axios.post('http://localhost:5000/exercises/update/' + this.props.match.params.id, exercise)
//       .then(res => console.log(res.data));

//     window.location = '/';
//   }

//   render() {
//     return (
//     <div>
//       <h3>Edit Exercise Log</h3>
//       <form onSubmit={this.onSubmit}>
//         <div className="form-group"> 
//           <label>Username: </label>
//           <input  type="text"
//               required
//               className="form-control"
//               value={this.state.username}
//               onChange={this.onChangeUsername}
//               />
//         </div>
//         <div className="form-group"> 
//           <label>Description: </label>
//           <input  type="text"
//               required
//               className="form-control"
//               value={this.state.description}
//               onChange={this.onChangeDescription}
//               />
//         </div>
//         <div className="form-group">
//           <label>Duration (in minutes): </label>
//           <input 
//               type="text" 
//               className="form-control"
//               value={this.state.duration}
//               onChange={this.onChangeDuration}
//               />
//         </div>
//         <div className="form-group">
//           <label>Date: </label>
//           <input 
//               type="text" 
//               className="form-control"
//               value={this.state.date}
//               onChange={this.onChangeDate}
//               />
//         </div>

//         <div className="form-group">
//           <input type="submit" value="Edit Exercise Log" className="btn btn-primary" />
//         </div>
//       </form>
//     </div>
//     )
//   }
// }
