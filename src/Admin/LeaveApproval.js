import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { Button } from 'reactstrap';




export default class LeaveApproval extends Component {
  constructor(props) {
    super(props);

  
    this.state = {exercises: [],databyId:[],ID:'',click:false,clicky:false,
    _id:'',
    leavetype:'',
    fromdate:'',
    status:'',
    todate:'',
    remarks:''
  }
    this.handleSubmit=this.handleSubmit.bind(this)
  }
 
  handleSubmit= (Id)=>
  { 
      axios.get(`http://localhost:5000/users/${Id}`).then(res=>
      {
        
       
        this.setState({
          databyId:res.data,
          ID:this.state.databyId._id,
          click:true
      }); 
    })
}

handleReject=(Id)=>
{
  axios.get(`http://localhost:5000/users/${Id}`).then(res=>
  {
     this.setState({
      databyId:res.data,
      ID:this.state.databyId._id,
      clicky:true
  }); 
})
}


  componentDidMount() {
    
    axios.get('http://localhost:5000/users/')
      .then(response =>
        {
          console.log(response.data);
          
        this.setState({ exercises: response.data })
      })
      .catch((error) => {
        console.log(error);
      })
  }

  setting()
  {
    
    const id= this.state._id; 
 const dataq={
        _id:this.state._id,
        leavetype:this.state.leavetype,
        fromdate:this.state.fromdate,
        status:this.state.status,
        todate:this.state.todate,
        remarks:this.state.remarks
      }
      axios.put(`http://localhost:5000/users/update/${id}`,dataq).then(res=>{
      console.log(res);
        window.location.reload();
      })
    }
  
  

  render() {
    
   if(this.state.click==true)
   {
    this.setState({
      _id:this.state.databyId._id,
      leavetype:this.state.databyId.leavetype,
       fromdate:this.state.databyId.fromdate,
      status:"Approved",
      todate:this.state.databyId.todate,
      remarks:this.state.databyId.remarks
    },()=>{this.setting()});
      
   }
   if(this.state.clicky==true)
   {
    this.setState({
      _id:this.state.databyId._id,
      leavetype:this.state.databyId.leavetype,
       fromdate:this.state.databyId.fromdate,
      status:"Rejected",
      todate:this.state.databyId.todate,
      remarks:this.state.databyId.remarks
    },()=>{this.setting()});
      
   }
    

      const {exercises}=this.state
        const data= exercises.map(currentexercise=> {
            return(
                <tr key={currentexercise._id}>
                    <td>{currentexercise.leavetype}</td>
                    <td>{currentexercise.fromdate}</td>
                    <td>{currentexercise.todate}</td>
                    <td>{currentexercise.remarks}</td>
                    <td>{currentexercise.status}</td>
                    <td>
                    <Button color="warning" onClick={()=>this.handleSubmit(currentexercise._id)} >Approval</Button> <Button color="warning" onClick={()=>this.handleReject(currentexercise._id)}>Reject</Button>
                    </td> 
              </tr>)
            }
        )
    return (
      <div>
      <Button color="warning" className="buttoninleaveapproval"><Link to ="/frontpage"> LogOut</Link> </Button>{' '}
         <h3>Leave Approval</h3>
<br></br>
        <table className="table">
          <thead className="thead-light">
            <tr>
              <th>LeaveType</th>
              <th>From Date</th>
              <th>ToDate</th>
              <th>Remarks</th>
              <th>Status</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {data}
          </tbody>
        </table>
      </div>
    )
  }
}